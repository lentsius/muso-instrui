extends Camera

var rapideco = 0.5

var muso = Vector2()

func _ready():
	Input.set_mouse_mode(2)

func _process(delta):
	rotate_y(-muso.x * rapideco * delta)
	rotate_object_local(Vector3(1,0,0), -muso.y * rapideco * delta)
	muso = Vector2()
	
func _input(event):
	if event is InputEventMouseMotion:
			muso = event.relative
